# New Vhost script, Version: 2.1.0b

New Vhost creation script, version 2!

This is a nearly complete rewrite of the older PHP version of this script.

Now, create your new virtual hosts with ease, and even choose which way to proxy them through PHP-FPM and FastCGI Proxy.

- Also based on the original idea at: https://gist.github.com/fideloper/2710970

This version packs more features and capabilities over those previous versions.

Requires (more like, recommends): PHP 7.3 or >, access to the BASH environment, Apache2.4 SAPI, Suexec - if you wish to "sandbox" CGI scripts, and PHP-FPM - to "sandbox" PHP scripts.

A built-in, detailed "help" text feature, so that learning to use this fine script becomes much easier.

Suggested installation to /usr/sbin folder, and then "chmod 0750" - to make it only visible and accessible to "root" or "sudo" users.

This script was designed to be used in a Linux environment, via CLI, and expects Apache2 to be running (although, the New Vhost 2 script will still create your necessary configuration files).

To use with "suexec" and/or "php-fpm",

Be sure the necessary libraries are installed (particular to "*suexec-custom*", especially) if you wish to use these features for running your vhosts' scripts as different users from that of the web-server.

Other than that,

This script should be self-explanatory and fairly easy to learn.

