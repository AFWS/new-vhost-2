#!/usr/bin/env php
<?php   //  ۞ text{ encoding:utf-8; bom:no; linebreaks:unix; tabs:4; }  ۞//

/*
 * APPLICATION TITLE: New Vhost 2
 * AUTHOR: Jim S. Smith
 * COPYRIGHT: (c)2022 - A FRESH WEB SOLUTION, Licensed, with attribution, under GPL 2.0 or later.
 *
 * VERSION: 2.1.0b, Updated: 2022/11/05
 *
 * DESCRIPTION: A CLI script to help a computer-user create new starter virtual hosts
 *     on a system running Apache2 SAPI, and PHP.
 *
 * USE: Create new virtual hosts.
 *
 * NOTE: Must run this application as sudo!
 *
 * You can move this file to /usr/local/bin and run command 'new-vhost.php' from anywhere, using sudo.
 *
 * OTHER NOTES: To make sure only root users (or those with 'sudo' privileges) can access this utility:
 *     This script should be placed in /usr/sbin or /usr/share/sbin, and chmod'd to '0750'. You could
 *     also rename it to just "new-vhost", or whatever, if you like. The PHP interpreter will still
 *     execute this script - PROVIDED the very first line is the same: The "shebang"-line, that is.
 */


/*	MINIMUM USER AND GROUP ID'S ALLOWED FOR WEB-ACCESS. (These can be changed as needed.)	*/
define( 'MIN_USER_ID', 1000 );
define( 'MIN_GROUP_ID', 1000 );


/* *    APPLICATION INFO.   * */
$THIS_APP = [
    'basename'      =>  basename( __FILE__ ),
    'version'       =>  '2.1.0b',
    'build_date'    =>  '20221105',
    ];

//  Script header info.
$THIS_APP['header'] = "{$THIS_APP['basename']} {$THIS_APP['version']}";


//  New Vhost Info.
$newVhostArray = [
    'server_name'   =>  '',
    'server_ip'     =>  '',
    'server_port'   =>  0,
    'server_alias'  =>  '',
//  'server_admin'  =>  '',

    'document_root' =>  '',

    'this_user'     =>  '',
    'this_group'    =>  '',

    'is_secured'    =>  false,
    'cert_path'     =>  '',
    'cert_name'     =>  '',

    'is_extra'      =>  '',
    'add_suexec'    =>  false,
    'add_php_fpm'   =>  false,

    'listen_port'   =>  0,
    ];


//  Display built-in "help"-text.
function show_help( $appData ) {
    fwrite( STDERR, <<< _HELP_
{$appData['header']}, Build Date: {$appData['build_date']}

{$appData['basename']} - Is a fairly advanced tool that lets you create new vHosts on a Linux Server, with ease!
( Assumes the default /etc/apache2/sites-available and /etc/apache2/sites-enabled setup is used. )

EXAMPLE: <sudo> {$appData['basename']} -d /var/www/yoursite -n my-test-server.com -i 192.168.100.2 -a test-server.local -g group1 -u user1

( -h for help, anywhere, will stop execution and display this help text. )


FLAGS/OPTIONS:

 !  -d / --document-root   Document Root - i.e. /var/www/yoursite ( Actually, it is more like the 'web-root'. )
                             without the 'htdocs' or other public folder as it will automatically be created.

 !  -n / --server-name     Server Name - i.e. example.com or sub.example.com

    -i / --server-ip       Server IP Address to assign this (virtual) domain, without the port (:nnnn) suffix.
                             i.e. 127.0.0.15

                             Defaults to '127.0.0.1' (localhost address) if not set.

    -x / --server-port     Server Port - Adds the port designation to the end of the IP Address. Number MUST be
                             within: 80 - 65535, and NO need to add a ':' - as this will be done automatically.
                             ( As such in examples:  127.0.0.15:8080, and 192.168.10.5:555 )

                             Defaults to '80' for unsecured set up, and '443' for secured SSL set up if not set.

    -a / --server-alias    Server Alias - i.e. *.example.com or another domain altogether
                             Multiple aliases list needs to be enclosed in single quotes and separated by
                             ONE space between them ( like: 'test.org test2.com new-server.com' )

 !  -u / --user-name       User Name or UID this domain belongs to.

    -g / --group-name      Group Name or GID this domain belongs to.

    -s / --secure          Set up for an SSL-secured server [ No value follows. ].
                             ( If this option is omitted, the '-p' and '-c' options are ignored. )

    -p / --cert-path       File path to the SSL certificate. Directories only, no file name.
                             If using an SSL Certificate, also creates a port :443 vhost as well.
                             This *ASSUMES* a .crt and a .key file exists at file path:
                               /provided-file-path/your-server-or-cert-name.[crt|key].
                             Otherwise you may have Apache errors when you reload Apache.
                             Ensure Apache's mod_ssl is enabled via "sudo a2enmod ssl".

                             Defaults to '/etc/ssl', if not set.

    -c / --cert-name       Name of certificate file ( without the .pem, .crt, .key extensions, etc. )
                             For example: "mysite.com" uses "mysite.com.key" and "mysite.com.crt".

                             Defaults to the Server Name, if not set.

    -e / --extra-options   Set one or more "extra options" within the list to enabled, and create and use
                             the needed config files. The list of sub-options needs a space separating each item,
                             and the entire list enclosed in single-quotes if more than one extra-option is selected.

                             EG: -e 'option1 option2 option3', --extra-options this-option

                             The two available extra-options: ( Both are used to run your scripts as a different user
                                and group from those of the running SAPI. )

                                 'suexec'   -   Enable the use of Suexec's features if mod_suexec is enabled and properly-
                                    configured. ( Be VERY sure to thoroughly check your configuration settings BEFORE using
                                    this option! )

                                 'php-fpm'  -   Enable the use of PHP-FPM ( PHP Fast Program Manager - Must have mod_proxy_fcgi
                                    available and enabled for this to work. ) Defaults to using 'mod_php' if not given.

                             There are no defaults if this option is enabled. - YOU must specify which available ones
                                you want enabled.

    -l / --listen-port     Listen(ing) Port. If this option is selected, the proxy-addressing will use "Ip-Address:Port"
                             format instead of the usual UNIX-Socket format. This option only applies if the "Extra" option:
                             'php-fpm' is selected. Otherwise, this option will have no effect.

                             NOTE: The chosen port must not already be in use by another listener.

    -h / --help            Help - Show this screen.


NOTE:      Arguments marked with a '!' are mandatory arguments.

EXAMPLE 2: To serve files from /var/www/mysite at http(s)://192.168.33.10
            using ssl files from /etc/ssl/mysite.com.[key|crt] :

<sudo> {$appData['basename']} -d /var/www/mysite -n 192.168.33.10 -p /etc/ssl -c mysite.com -s

EXAMPLE 3:  To use "long" options ( can be used as '--this-option=this-value' or '--that-option another-value' ) :

<sudo> {$appData['basename']} --document-root=/var/webroot --server-name myhomeblog.site --server-ip 192.168.45.200 -p /etc/ssl -c my-home-blog
            --secure <no value>

( Yes, mixed entry types will work also! )


_HELP_
    );

    die();
}   //  NOTE: ** - Can be IPv4 or IPv6. [ IPv6 is not yet fully supported. ]

function show_usage() {
    $this_app = basename( __FILE__ );

    fwrite( STDERR, <<< _HELP_
\033[07;33m\033[1mEXAMPLE:\033[0m <sudo> $this_app -d /var/www/yoursite -n my-test-server.com -i 192.168.100.2 -a test-server.local -g group1 -u user1

\033[17;33m\033[1m ( $this_app -h or --help, for more help on valid options. ) \033[0m


_HELP_
    );

    die();
}

//	Colored Error messages!
function post_errors( $this_message ) {
    $this_app = basename( __FILE__ );

    fwrite( STDERR, <<< _TEXT_
\033[06;31m\033[1m ERROR: \033[0m \033[17;31m\033[1m $this_message \033[0m

\033[07;33m\033[1m $this_app --help (or '-h') for info. \033[0m


_TEXT_
    );

    die();
}

//  Send shell commands, and retrive output results.
function send_to_shell( $cmd_str = '' ) {
    ob_start();
    system( "$cmd_str" );
    $return_code = trim( ob_get_clean() );

    if ( empty( $return_code ) ) {
        return 0;
        }

    return $return_code;
}

function auth_check() {
    if ( send_to_shell( 'id -u' ) ) {
        post_errors( "NEED TO BE 'ROOT'-USER IN ORDER TO RUN THIS!" );
        }

    return true;
}

function getUserAndGroupIDs( $this_user = 'www-data' ) {
	if ( ! ( $user_data = file_get_contents( '/etc/passwd') ) ) {
		post_errors( "Could not read the system 'passwd' file!" );
		}

	if ( strpos( $user_data, "{$this_user}:x" ) === false ) {
		post_errors( "User '$this_user' does not exist on this system!" );
		}

	if ( preg_match( "/{$this_user}:x:([0-9]+):([0-9]+):/", $user_data, $m ) ) {
		return [ 'UID' => $m[1], 'GID' => $m[2] ];
		}

	return false;
}

function getGroupInfo ( $this_group = 'www-data' ) {
	if ( ! ( $group_data = file_get_contents( '/etc/group') ) ) {
		post_errors( "Could not read the system 'group' file!" );
		die();
		}

	if ( strpos( $group_data, "{$this_group}:x" ) === false ) {
		post_errors( "Group '$this_group' does not exist on this system!" );
		die();
		}

	if ( preg_match( "/{$this_group}:x:([0-9]+):/", $group_data, $m ) ) {
		return "{$m[1]}";
		}

	return false;
}

function regex_check( $pattern, $test_data ) {
    if ( preg_match( "#([^$pattern]+)#i", $test_data, $match ) ) {
//  return implode( ',', $match );
        return $pattern;
        }

    return false;
}

//  Test host-address to see if it is properly-formatted by version-type.
function strict_ip_address( $server_ip ) {
    if ( preg_match( '#^\d{1,3}(\.\d{1,3}){3}$#', $server_ip ) && ! preg_match( '#[^0-9.]+#', $server_ip ) ) {
        return 4;
        }

    elseif ( preg_match( '#[0-9a-f:]+#i', $server_ip ) && ! preg_match( '#[^0-9a-f:]+#', $server_ip ) ) {
        return 6;
        }

    return false;
}

//  Return a properly-formatted host-address, based in its IP-version type.
function set_vhost_address( $server_ip, $server_port ) {
    if ( $ipType = strict_ip_address( $server_ip ) ) {
        if ( $ipType == 6 ) {
            return "[$server_ip]:$server_port";
            }

        elseif ( $ipType == 4 ) {
            return "$server_ip:$server_port";
            }
    }

    return false;
}

function test_for_public_folder( $folder_path = '' ) {
    if ( empty( $folder_path ) ) {
        return false;
        }

//  Test against the most common defaults for "public folders".
    if ( preg_match( '#/(htdocs|html|public((_|-)(html|data|web))?|www(_|-)(public|data)|web(-data)?)/?$#i', $folder_path, $m ) ) {
        return $m[1];
        }

    return false;
}

function get_webroot_folder( $folder_path = '' ) {
    if ( test_for_public_folder( $folder_path ) ) {
        return dirname( $folder_path );
        }

    return rtrim( $folder_path, '/' );
}

function get_public_folder( $folder_path = '' ) {
    if ( test_for_public_folder( $folder_path ) ) {
        return rtrim( basename( $folder_path ) );
        }

    return 'htdocs';
}

function create_ht_hdr( $ht_hdr = '', $ht_for = '', $_c = '#', $thisInfo = '' ) {
    if ( empty( $ht_hdr ) || empty( $ht_for ) ) {
        return '';
        }

    return <<< _CODE_
$_c New $ht_hdr file for: $ht_for
$_c Generated by: $thisInfo


_CODE_;
}

//  Test if the private key certificate exists for that host name.
function is_private_key( $vhostArray ) {
    if ( empty( $vhostArray['cert_path'] ) || empty( $vhostArray['cert_name'] ) ) {
        return '';
        }

    foreach ( [ "{$vhostArray['cert_name']}.key", "private/{$vhostArray['cert_name']}.key", "{$vhostArray['cert_name']}.pem", "private/{$vhostArray['cert_name']}.pem" ]
        as $this_cert ) {

        if ( file_exists( "{$vhostArray['cert_path']}/$this_cert" ) ) {
            return "{$vhostArray['cert_path']}/$this_cert";
            }
        }

    return '';
}

//  Test if the SSL certificate exists for that host name.
function is_certificate( $vhostArray ) {
    if ( empty( $vhostArray['cert_path'] ) || empty( $vhostArray['cert_name'] ) ) {
        return '';
        }

    foreach ( [ "{$vhostArray['cert_name']}.crt", "certs/{$vhostArray['cert_name']}.crt", "{$vhostArray['cert_name']}.pem", "certs/{$vhostArray['cert_name']}.pem" ]
        as $this_cert ) {

        if ( file_exists( "{$vhostArray['cert_path']}/$this_cert" ) ) {
            return "{$vhostArray['cert_path']}/$this_cert";
            }
        }

    return '';
}

function warn_no_cert( $vhostArray ) {
    die( <<< _MSG_

NOTE:  Either '{$vhostArray['cert_path']}/{$vhostArray['cert_name']}.key', or '{$vhostArray['cert_path']}/{$vhostArray['cert_name']}.crt' is missing!
       From the current choice of arguments, this will result in a server error upon restart.

       Please recheck where your certificates are stored, or generate new ones for this new host ({$vhostArray['server_name']}).


_MSG_
    );

/*
 * NOTE:  Sometimes this is how the certificates' folders are arranged?
 *
 * private key ==> /etc/ssl/private
 * certificate ==> /etc/ssl/certs -> ( Occasionally: just in '/etc/ssl' )
 */

}

//  Create htconfig entry for "suexec" if "suexec"-option was selected.
function gen_suexec_htdata( $vhostArray ) {
    return <<< _CODE_

# Set up SuExec security model for running CGI scripts to the proper user/group.
    <IfModule mod_suexec.c>
        SuexecUserGroup {$vhostArray['this_user']} {$vhostArray['this_group']}
        UserDir enabled
        UserDir {$vhostArray['document_root']}
    </IfModule>

_CODE_;
}

//  Create user's "suexec" config file - if it does not exist.
function gen_suexec_conf( $vhostArray ) {
    if ( file_exists( "/etc/apache2/suexec/{$vhostArray['this_user']}" ) ) {
        return '';
        }

    $webRoot = dirname( $vhostArray['document_root'] );
    $userDir = basename( $vhostArray['document_root'] );

    return <<<_CODE_
$webRoot
$userDir
# The first two lines contain the suexec document root and the suexec userdir
# suffix. If one of them is disabled by prepending a # character, suexec will
# refuse the corresponding type of request.
# This config file is only used by the apache2-suexec-custom package. See the
# suexec man page included in the package for more details.
#
# This file is for: '{$vhostArray['this_user']}'
#
_CODE_;
}

//  Search for running instance of PHP-FPM Daemon and return its version info.
function find_phpfpm() {
    return send_to_shell( 'which_one=$(ps -ef) ; echo "$which_one" | grep -P "php-fpm"' );
}

//  Get PHP version (and reduce to major and minor version numbers).
function find_php_version() {
    if ( ( $_data = find_phpfpm() ) && preg_match( '#/etc/php/([0-9.]+)/fpm/php-fpm\.conf#', $_data, $m ) ) {
        }
    elseif ( preg_match( '#^(\d+\.\d+)\..*#', PHP_VERSION, $m ) ) {
        }
    else {
        return false;
        }

    return $m[1];
}

//  Determine if the desired "listen-port" already has a listener.
function isPort_used( $port_no = 0 ) {
    $port_no = ( preg_match( '#[^0-9]+#', $port_no ) ? false : (int)$port_no );

    if ( $port_no ) {
        return send_to_shell( <<< _SHELL_
for P in \$(ss -tuln | grep LISTEN | grep -P -o ':([0-9]+)\ ' | grep -P -o '\d+') ; do
if [ $port_no -eq \$P ] ; then
echo \$P
exit
fi
done

echo 0
_SHELL_
        );
        }

    return $port_no;
}

//  See if this host already exists in the system "hosts" file.
function check_hosts_file( $vhostArray ) {
    $server_alias = ( $vhostArray['server_alias'] ? " {$vhostArray['server_alias']}" : '' );
    return strpos( file_get_contents( '/etc/hosts' ), "{$vhostArray['server_ip']} {$vhostArray['server_name']}$server_alias" );
}

//  Add this host into the system "hosts" file.
function update_hosts_file( $vhostArray, $appInfo = '' ) {
    $server_alias = ( $vhostArray['server_alias'] ? " {$vhostArray['server_alias']}" : '' );

    if ( $hosts_data = file_get_contents( '/etc/hosts' ) ) {
        $hosts_data .= "# Added by: $appInfo.\n{$vhostArray['server_ip']} {$vhostArray['server_name']}$server_alias\n\n";
        return file_put_contents( '/etc/hosts', $hosts_data );
        }

    return false;
}

//  TO REMOVE HOSTS ENTRIES:
//
//  $server_aliases = str_replace( ' ', '\h+', $server_alias );
//  $hosts_data = preg_replace( "!#\h+Added\h+by:\h+$MYINFO\.\n$server_ip\h+$server_name$server_alias\n\n!i", '', $hosts_data );
//

function gen_ssl_htdata( $vhostArray ) {
    if ( $vhostArray['is_secured'] ) {
        if ( ! ( $vhostArray['certificate_path'] = is_certificate( $vhostArray ) ) ||
            ! ( $vhostArray['private_key_path'] = is_private_key( $vhostArray ) )
            ) {

            warn_no_cert( $vhostArray );
            return '';
            }

        return <<< _CODE_

# Enable SSl encryption if it is supported.
    <IfModule mod_ssl.c>
        SSLEngine on
        SSLCertificateFile    {$vhostArray['certificate_path']}
        SSLCertificateKeyFile {$vhostArray['private_key_path']}

        <FilesMatch "\\.(cgi|(p|s)html|php|pl|py|rb|tcl|aspx?|jspx?)$">
            SSLOptions +StdEnvVars
        </FilesMatch>

        # Handle old problem browsers.
        BrowserMatch "MSIE [2-6]" \\
            nokeepalive ssl-unclean-shutdown \\
            downgrade-1.0 force-response-1.0

        # MSIE 7 and newer should be able to use keepalive
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
    </IfModule>
_CODE_;
        }

    return '';
}

//  Generate PHP ini-settings entries list.
function gen_php_ini_settings( $vhostArray, $_tabs = 0 ) {
    $pool = str_replace( '.', '_', $vhostArray['server_name'] );
    $this_tmp = dirname( $vhostArray['document_root'] ) . '/.tmp';
    $this_log = dirname( $vhostArray['document_root'] ) . '/.logs';

    $isFPM_mode = $vhostArray['add_php_fpm'];

    $spacer = '';

    if ( $_tabs ) {
        $spacer = str_repeat( " ", $_tabs * 4 );
        }

    $php_settings = [
        'env-HOSTNAME'  =>  "{$vhostArray['server_name']}",
        'env-HOME'      =>  "{$vhostArray['document_root']}",
        'env-PATH'      =>  "/usr/local/bin:/usr/bin:/bin",

        'php_admin_value-sendmail_path'             =>  "/usr/sbin/sendmail -t -i -f {$vhostArray['this_user']}@{$vhostArray['server_name']}",
        'php_flag-display_errors'                   =>  "off",
        'php_admin_value-error_log'                 =>  "$this_log/php_errors.log",
        'php_admin_flag-log_errors'                 =>  "on",
        'php_admin_value-memory_limit'              =>  "64M",
        'php_admin_value-session.save_path'         =>  "$this_tmp",
        'php_admin_flag-session.use_strict_mode'    =>  1,
        'php_admin_flag-session.use_cookies'        =>  1,
        'php_admin_flag-session.use_only_cookies'   =>  1,
        'php_admin_value-session.name'              =>  "SESS_$pool",
        'php_admin_flag-session.auto_start'         =>  "no",
        'php_admin_value-session.cookie_lifetime'   =>  0,
        'php_admin_value-session.cookie_domain'     =>  "{$vhostArray['server_name']}",
        'php_admin_value-session.cookie_path'       =>  "/",
        'php_admin_flag-session.cookie_httponly'    =>  1,
        'php_admin_value-session.cookie_samesite'   =>  "Strict",
        'php_admin_flag-session.cookie_secure'      =>  "off",
//      'php_admin_value-auto_prepend_file'         =>  "",
        //''    =>  '',
        ];

    $ini_entries = '';

    foreach ( $php_settings as $this_field => $the_setting ) {
        $field_entry = explode( '-', $this_field );

        if ( strpos( $field_entry[0], 'php_' ) !== 0 && ! $isFPM_mode ) {
            continue;
            }

        $ini_entries .= $spacer;

        if ( $isFPM_mode ) {
            $ini_entries .= "{$field_entry[0]}[{$field_entry[1]}] = ";
            }
        else {
            $ini_entries .= "{$field_entry[0]} {$field_entry[1]} ";
            }

        if ( strpos( $field_entry[0], '_flag' ) !== false ) {
            $ini_entries .= "$the_setting";
            }
        else {
            $ini_entries .= "'$the_setting'";
            }

        $ini_entries .= "\n";
        }

    return $ini_entries;
}

//  Create new host's PHP-FPM "pool" config file.
function gen_phpfpm_pool( $vhostArray ) {
    echo "\nGenerating PHP-FPM 'pool' config for: '{$vhostArray['server_name']}'.\n";

    $pool = str_replace( '.', '_', $vhostArray['server_name'] );

    if ( $vhostArray['listen_port'] && $vhostArray['server_ip'] ) {
        $listen_here = set_vhost_address( $vhostArray['server_ip'], $vhostArray['listen_port'] );
        }
    else {
        $listen_here = "/var/run/php/$pool-fpm.sock";
        }

    if ( empty( $vhostArray['this_group'] ) ) {
        $vhostArray['this_group'] = $vhostArray['this_user'];
        }

    $php_settings = gen_php_ini_settings( $vhostArray );

    return <<< _CODE_
; pool name (EG: 'www' here)
[$pool]

user = {$vhostArray['this_user']}
group = {$vhostArray['this_group']}

listen = $listen_here

; Default Values: listen.user and listen.group are set as the running user
;    listen.mode is set to 0660
listen.owner = www-data
listen.group = www-data
;listen.mode = 0660

pm = ondemand
pm.max_children = 10
pm.process_idle_timeout = 10s;
pm.max_requests = 200

access.log = /var/log/apache2/$pool.proxy.log
access.format = "%{REMOTE_ADDR}e - %u [%t] \\"%m %r%Q%q\\" %s %l %f PPID(%P) CPID(%p) %{mili}d %{kilo}M %C%%"

clear_env = yes

security.limit_extensions = '.php'

;env[TMP] = '/tmp'
;env[TMPDIR] = '/tmp'
;env[TEMP] = '/tmp'

$php_settings
_CODE_;
}

//  Create ht-config entry for mod_php usage.
function gen_modphp_htdata( $vhostArray ) {
    $pool = str_replace( '.', '_', $vhostArray['server_name'] );
    $version = (int)find_php_version();
    $php_settings = gen_php_ini_settings( $vhostArray, 3 );

    return <<< _CODE_

    <Directory {$vhostArray['document_root']}>
        <IfModule mod_php$version.c>
            <FilesMatch "\\.ph(ar|ps|tml)\$">
                Require all denied
            </FilesMatch>

            <Files "\\.php\$">
                SetHandler application/x-httpd-php
            </Files>

$php_settings
        </IfModule>

        <IfModule !mod_php$version.c>
            <FilesMatch "\\.ph(ar|ps?|tml)\$">
                Require all denied
            </FilesMatch>
        </IfModule>
    </Directory>

_CODE_;
}

//  Create ht-config data for using PHP-FPM in FastCGI proxy.
function gen_phpfpm_htdata( $vhostArray ) {
    $pool = str_replace( '.', '_', $vhostArray['server_name'] );
    $version = (int)find_php_version();

    if ( $vhostArray['listen_port'] && $vhostArray['server_ip'] ) {
        $listen_here = "fcgi://" . set_vhost_address( $vhostArray['server_ip'], $vhostArray['listen_port'] );
        }
    else {
        $listen_here = "unix:/var/run/php/$pool-fpm.sock|fcgi://localhost";
        }

    return <<< _CODE_

    <IfModule mod_proxy_fcgi.c>
        <IfModule setenvif_module>
            SetEnvIfNoCase ^Authorization\$ "(.+)" HTTP_AUTHORIZATION=\$1
        </IfModule>

        <FilesMatch "\\.ph(ar|p|tml)\$">
            <If "-f %{REQUEST_FILENAME}">
                SetHandler "proxy:$listen_here"
            </If>
        </FilesMatch>
    </IfModule>

    <IfModule !mod_proxy_fcgi.c>
        <IfModule mod_php$version.c>
            <FilesMatch "\\.ph(ar|p|tml)\$">
                SetHandler application/x-httpd-php
            </FilesMatch>
        </IfModule>

        <IfModule !mod_php$version.c>
            <FilesMatch "\\.ph(ar|ps?|tml)\$">
                Require all denied
            </FilesMatch>
        </IfModule>
    </IfModule>

_CODE_;
}

//  Create directory-access containers.
function gen_dir_data( $vhostArray ) {
    $web_root = dirname( $vhostArray['document_root'] );

    return <<< _CODE_
# Set Directory-Access, location, and limitations.
    <Directory "$web_root">
        Options none
        AllowOverride All
        Require all denied
    </Directory>

    <Directory "{$vhostArray['document_root']}">
        Options -Indexes +SymLinksIfOwnerMatch -MultiViews +ExecCGI +Includes
        DirectoryIndex index.html index.htm index.php index.cgi
        IndexIgnore *
        AddHandler cgi-script .cgi .pl .py
        AllowOverride None
        Require all granted
    </Directory>
_CODE_;
}

//  Create/Generate the complete virtual host server config file.
function create_vhost( $vhostArray ) {
    echo "\nGenerating vHost-configuration for: '{$vhostArray['server_name']}'.\n";

    $server_alias = ( ! empty( $vhostArray['server_alias'] ) ? "ServerAlias  {$vhostArray['server_alias']}\n" : '' );
    $this_log = dirname( $vhostArray['document_root'] ) . '/.logs';

//  Use for properly-formatted IP addresses (whether IPv4 or IPv6).
    $host_address = set_vhost_address( $vhostArray['server_ip'], $vhostArray['server_port'] );

    $genSSL = ( $vhostArray['is_secured'] ? gen_ssl_htdata( $vhostArray ) : '' );
    $suexec = ( $vhostArray['add_suexec'] ? gen_suexec_htdata( $vhostArray ) : '' );
    $phpfpm = "\n# Set PHP handler based on which module is active." . ( $vhostArray['add_php_fpm'] ? gen_phpfpm_htdata( $vhostArray ) : gen_modphp_htdata( $vhostArray ) );
    $dirAccess = gen_dir_data( $vhostArray );

    return <<< _CODE_
# Create the new virtual host with its own IP address and port.
<VirtualHost $host_address>
    ServerName   {$vhostArray['server_name']}
    ServerAdmin  {$vhostArray['this_user']}@{$vhostArray['server_name']}
    $server_alias
    DocumentRoot {$vhostArray['document_root']}

    ErrorLog $this_log/error.log
    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn

    CustomLog $this_log/access.log combined
$genSSL
$suexec$phpfpm
$dirAccess
</VirtualHost>

_CODE_;
}

function create_special_folders( $vhostArray, $appData ) {
    $document_root = rtrim( $vhostArray['document_root'], '/' );
    $web_root = dirname( $document_root );

    send_to_shell( "mkdir -p -m 0755 $document_root" );

    file_put_contents( "$web_root/.htaccess", create_ht_hdr( '.htaccess', "$web_root on: {$vhostArray['server_name']}.", '#', $appData ) );
    chmod( "$web_root/.htaccess", 0755 );

    file_put_contents( "$document_root/.htaccess", create_ht_hdr( '.htaccess', "$document_root on: {$vhostArray['server_name']}.", '#', $appData ) );
    chmod( "$document_root/.htaccess", 0755 );

    foreach ( [ '.cache', '.etc', '.logs', '.tmp' ] as $sub_dir ) {
        if ( ! file_exists( "$web_root/$sub_dir" ) ) {
            mkdir( "$web_root/$sub_dir", 0750 );
            }

        file_put_contents( "$web_root/$sub_dir/._placeholder_",
            create_ht_hdr( 'place-holder', "$web_root/$sub_dir on: {$vhostArray['server_name']}.", '#', $appData ) );
        }
}

//  Create HTML index file (at least as a default file for the vHost).
function create_index( $vhostArray, $appTitle = 'This App' ) {
    return <<< _HTML_
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">

    <title>Welcome to {$vhostArray['server_name']}!</title>

    <style type='text/css'><!--
        h1 {text-align:center;color:green;}
        h2 {text-align:center;color:red;}
        p {text-align:center;color:blue;font-weight:bold;}
    --></style>
  </head>

  <body>
    <h1>Welcome to {$vhostArray['server_name']}!</h1>
    <h2>This IP Address is -&gt; {$vhostArray['server_ip']}</h2>
    <h3>- HTML file Created using: "$appTitle".</h3>
    <hr />
    <p>The easy way to create new domains and virtual domains on Apache2 Server!</p>
  </body>

</html>

_HTML_;
}

function create_test_py_old( $appTitle = 'This App' ) {
    return <<< _CODE_
#!/usr/bin/python
import os

print("Content-Type: text/html")
print()
print("<!DOCTYPE html>")
print("<html dir='ltr' lang='en-US'>")
print("<head>")
print("<meta name='charset' content='utf-8'>")
print("<title>Testing Python via CGI-Script Handler</title>")
print("<style type='text/css'><!--")
print("    h1 {text-align:center;color:green;}")
print("    h2 {text-align:center;color:red;}")
print("    p {text-align:center;color:blue;font-weight:bold;}")
print("--></style>")
print("</head>")
print("<body>")
print("<h1>Testing Python, accessed through CGI on:",os.environ['SERVER_NAME'] + ".</h1>")
print("<h2>This IP Address is -&gt; ",os.environ['SERVER_ADDR'],"</h2>")
print("<h3>- Created using: '$appTitle'.</h3>")
print("<hr>")
print("<p>And, . . . IT WORKS! ! ! (The 'Old-Fashioned Python way')</p>")
print("<div>",os.environ,"</div>")
print("</body>")
print("</html>")
print()

_CODE_;
}

function create_test_py( $appTitle = 'This App' ) {
    return <<< _CODE_
#!/usr/bin/python
from os import environ
from subprocess import check_output, STDOUT

thisUser = check_output('id -un', stderr=STDOUT, shell=True, universal_newlines=True)
thisGroup = check_output('id -gn', stderr=STDOUT, shell=True, universal_newlines=True)

out = """Content-Type: text/html

<!DOCTYPE html>

<html dir='ltr' lang='en-US'>
    <head>
    <meta name='charset' content='utf-8'>

    <title>Testing Python via CGI-Script Handler</title>

    <style type='text/css'><!--
         h1 {text-align:center;color:green;}
         h2 {text-align:center;color:red;}
         p {text-align:center;color:blue;font-weight:bold;}
    --></style>
</head>

<body>
    <h1>Testing Python, accessed through CGI on: %s.</h1>
    <h2>This IP Address is -&gt; %s</h2>
    <h3>- Created using: "$appTitle".</h3>
    <hr>
    <p>And, . . . IT WORKS! ! !</p>

    <hr>

    <p>As USER: %s</p>
    <p>of GROUP: %s.</p>

    <hr>

    <div>%s</div>
</body>

</html>

""" % (environ['HTTP_HOST'], environ['SERVER_ADDR'], thisUser, thisGroup, environ)

print (out)

_CODE_;
}

function create_test_cgi( $appTitle = 'This App' ) {
    return <<< _CODE_
#!/bin/sh

cat << HTML
Content-Type: text/html

<!DOCTYPE html>
<html dir='ltr' lang='en-US'>

<head>
    <meta name='charset' content='utf-8'>

    <title>Testing BASH Script via CGI-Script Handler</title>

    <style type='text/css'><!--
        h1 {text-align:center;color:green;}
        h2 {text-align:center;color:red;}
        p {text-align:center;color:blue;font-weight:bold;}
    --></style>
</head>

<body>
    <h1>Testing BASH Script, accessed through CGI on: \$SERVER_NAME.</h1>
    <h2>This IP Address is: \$SERVER_ADDR</h2>
    <h3>- Created using -&gt; "$appTitle".</h3>
    <hr>
    <p>And, . . . IT WORKS! ! !</p>

    <hr>

    <p>As USER: \$(id -un)</p>
    <p>of GROUP: \$(id -gn).</p>

    <hr>

    <pre>ENV =<br><br>
\$(env)
    </pre>

</body>

</html>

HTML

_CODE_;
}

function create_test_php( $appTitle = 'This App' ) {
    return <<< _CODE_
<?php

\$env_data = var_export( \$_SERVER, true );

ob_start();
\$this_user = system( 'id -un' );
\$this_group = system( 'id -gn' );
ob_end_clean();

header( 'Content-Type: text/html' );

echo <<<HTML
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">

    <title>Welcome to {\$_SERVER['SERVER_NAME']}, from PHP!</title>

    <style type='text/css'><!--
        h1 {text-align:center;color:green;}
        h2 {text-align:center;color:red;}
        p {text-align:center;color:blue;font-weight:bold;}
    --></style>
  </head>

  <body>
    <h1>Welcome to {\$_SERVER['SERVER_NAME']}, from PHP!</h1>
    <h2>This IP Address is -&gt; {\$_SERVER['SERVER_ADDR']}.</h2>
    <h3>- Created using: "$appTitle"</h3>
    <hr />
    <p>The easy way to create new domains and virtual domains on Apache2 Server!</p>


    <hr>

    <p>As USER: \$this_user</p>
    <p>of GROUP: \$this_group.</p>

    <hr>

    <pre>\$env_data</pre>
  </body>

</html>

HTML;

?>

_CODE_;
}

function create_test_pl( $appTitle = 'This App' ) {
    return <<<_CODE_
#!/usr/bin/perl

sub send_to_shell {
    \$rc = '';

    open( CommandOutput, "\$_[0] |" );
    while( <CommandOutput> ) {
        \$rc .= "\$_";
        }

    \$rc =~ s/(\\n|\\r\\n)\$//;
    return \$rc;
}

\$this_user = &send_to_shell( "id -un" );
\$this_group = &send_to_shell( "id -gn" );

\$env_str = &send_to_shell( "env" );

print <<_HTML_
Content-Type: text/html

<!DOCTYPE html>
<html dir='ltr' lang='en-US'>

<head>
    <meta name='charset' content='utf-8'>

    <title>Testing PERL Script via CGI-Script Handler</title>

    <style type='text/css'><!--
        h1 {text-align:center;color:green;}
        h2 {text-align:center;color:red;}
        p {text-align:center;color:blue;font-weight:bold;}
    --></style>
</head>

<body>
    <h1>Testing PERL Script, accessed through CGI on: \$ENV{'SERVER_NAME'}.</h1>
    <h2>This IP Address is -&gt; \$ENV{'SERVER_ADDR'}</h2>
    <h3>- Created using: "$appTitle".</h3>
    <hr>
    <p>And, . . . IT WORKS! ! !</p>

    <hr>

    <p>As USER: \$this_user</p>
    <p>of GROUP: \$this_group.</p>

    <hr>

    <pre>ENV =<br><br>
\$env_str
    </pre>

</body>

</html>

_HTML_

_CODE_;
}

//  Create the different script-type "test" executables and vhost's "special folders".
function populate_public_folder( $vhostArray, $appData ) {
    echo "\nGenerating home files for: '{$vhostArray['server_name']}'.\n";

    $webRoot = dirname( $vhostArray['document_root'] );

    create_special_folders( $vhostArray, $appData );

    file_put_contents( "{$vhostArray['document_root']}/index.html", create_index( $vhostArray, $appData ) );
    file_put_contents( "{$vhostArray['document_root']}/test.cgi", create_test_cgi( $appData ) );
    file_put_contents( "{$vhostArray['document_root']}/test.py", create_test_py( $appData ) );
    file_put_contents( "{$vhostArray['document_root']}/test.pl", create_test_pl( $appData ) );
    file_put_contents( "{$vhostArray['document_root']}/test-2.py", create_test_py_old( $appData ) );
    file_put_contents( "{$vhostArray['document_root']}/test.php", create_test_php( $appData ) );
    file_put_contents( "{$vhostArray['document_root']}/info.php", '<?php phpinfo(); ?>' );

    return send_to_shell( <<< _SHELL_
chmod -R 0755 {$vhostArray['document_root']}
chown -R {$vhostArray['this_user']}:{$vhostArray['this_group']} $webRoot
_SHELL_
    );
}


/* * *  START OF MAIN PROGRAM.  * * */

//  It seems PHP also has a very nice "getopt" feature! - So we can also do long and/or short options.
$theseOptions = getopt( 'd:n:i:x:a:g:u:p:c:e:l:sh',
    [ 'document-root:', 'server-name:', 'server-ip:', 'server-port:', 'server-alias:', 'group-name:', 'user-name:', 'cert-path:', 'cert-name:', 'extra-options:', 'listen-port:', 'secure', 'help' ] );


//  The real magic of parsing commandline options is here!
foreach ( $theseOptions as $thisOpt => $optValue ) {

//  Make sure that each option argument is used only once in the commandline!
    if ( is_array( $optValue ) ) {
        $optValue = $optValue[0];
        }

    switch ( $thisOpt ) {

        case 'document-root':
        case 'd':
            if ( $test_pattern = regex_check( '_/a-z0-9-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Document Root!" );
                }
            elseif ( preg_match( '#/{2,}#', $optValue ) ) {
                post_errors( "Document Root has too many contiguous '/'s! '// +' is not allowed." );
                }
            elseif ( strpos( $optValue, '/' ) != 0 ) {
                post_errors( "Document Root must be an absolute path (with '/' as the first character)!" );
                }

            $newVhostArray['document_root'] = rtrim( $optValue, '/' );
        break;

        case 'server-name':
        case 'n':
            if ( $test_pattern = regex_check( 'a-z0-9.-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Server Name!" );
                }

            $newVhostArray['server_name'] = $optValue;
        break;

        case 'server-ip':
        case 'i':
            if ( $test_pattern = regex_check( 'a-f0-9.:', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Server IP!" );
                }
            if ( ! strict_ip_address( $optValue ) ) {
                post_errors( "'$optValue' is not valid for a Server IP!" );
                }
            if ( strict_ip_address( $optValue ) == 6 ) {
                post_errors( "'$optValue' - IPv6 is not yet fully supported for a Server IP. Please use an IPv4 address for now." );
                }

            $newVhostArray['server_ip'] = $optValue;
        break;

        case 'server-port':
        case 'x':
            if ( $test_pattern = regex_check( '0-9', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Server Port!" );
                }
            if ( (int)$optValue < 80 || (int)$optValue > 65535 ) {
                post_errors( "'$optValue' is outside the range of 80-65535 for Server Port!" );
                }

            $newVhostArray['server_port'] = trim( $optValue );
        break;

        case 'server-alias':
        case 'a':
            if ( $test_pattern = regex_check( '\ a-z0-9.-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Server Aliases list!" );
                }

            $newVhostArray['server_alias'] = trim( $optValue );
        break;

        case 'group-name':
        case 'g':
            if ( $test_pattern = regex_check( 'a-z0-9-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Group Name!" );
                }

            $newVhostArray['this_group'] = $optValue;
        break;

        case 'user-name':
        case 'u':
            if ( $test_pattern = regex_check( 'a-z0-9-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in User Name!" );
                }

            $newVhostArray['this_user'] = $optValue;
        break;

        case 'cert-path':
        case 'p':
            if ( $test_pattern = regex_check( '_/a-z0-9-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Certificate Path!" );
                }
            elseif ( preg_match( '#/{2,}#', $optValue ) ) {
                post_errors( "Certificate Path has too many contiguous '/'s! '// +' is not allowed." );
                }
            elseif ( strpos( $optValue, '/' ) != 0 ) {
                post_errors( "Certificate Path must be an absolute path (with '/' as the first character)!" );
                }

            $newVhostArray['cert_path'] = rtrim( $optValue, '/' );
        break;

        case 'cert-name':
        case 'c':
            if ( $test_pattern = regex_check( '_a-z0-9.-', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Certificate Name!" );
                }

            $newVhostArray['cert_name'] = $optValue;
        break;

        case 'extra-options':   //   Handle "extra option" switch.
        case 'e':
            $isExtra = $optValue;

            if ( $isExtra ) {
                if ( strstr( $isExtra, 'suexec' ) ) {
                    $newVhostArray['add_suexec'] = true;
                    }
                if ( strstr( $isExtra, 'php-fpm' ) ) {
                    $newVhostArray['add_php_fpm'] = true;
                    }
                }
        break;

        case 'listen-port':
        case 'l':
            if ( $test_pattern = regex_check( '0-9', $optValue ) ) {
                post_errors( "Only '$test_pattern' chars. are allowed in Listen Port!" );
                }
            if ( (int)$optValue < 80 || (int)$optValue > 65535 ) {
                post_errors( "'$optValue' is outside the range of 80-65535 for Listen Port!" );
                }
            if ( isPort_used( $optValue ) ) {
                post_errors( "'$optValue' already has a listerner. Must use another port-number!" );
                }

            $newVhostArray['listen_port'] = trim( $optValue );
        break;

        case 'secure':
        case 's':
            $newVhostArray['is_secured'] = true;
        break;

        case 'help':
        case 'h':
            show_help( $THIS_APP );
        break;

        default :
            post_errors( 'Unrecognized option used!' );

        }
    }

//  Sanity Check - are there at least two arguments with 2 values?
if ( @$argc < 3 && @count( $theseOptions ) < 3 ) {
    post_errors( 'Too few arguments! Need at least three to work with.' );
    }

auth_check();

//  We MUST have BOTH 'Document Root' AND 'Server Name' at the minimum.
if ( ! $newVhostArray['document_root'] || ! $newVhostArray['server_name'] ) {
    post_errors( 'Both the document root AND server name are required!' );
    }

if ( ! $newVhostArray['this_user'] || empty( trim( $newVhostArray['this_user'] ) ) ) {
	post_errors( 'You MUST specify an existing User-name which has an ID of at least ' . MIN_USER_ID . '!' );
	}

if ( ! $newVhostArray['this_group'] || empty( trim( $newVhostArray['this_group'] ) ) ) {
	$newVhostArray['this_group'] = $newVhostArray['this_user'];
	}

echo "\nCreating for: '{$newVhostArray['this_user']}:{$newVhostArray['this_group']}':\n\n";

if ( ! ( $this_users_IDs = getUserAndGroupIDs( $newVhostArray['this_user'] ) ) ) {
	post_errors( "Could not get data on User '{$newVhostArray['this_user']}'!" );
	}
echo "In '/etc/passwd', found User '{$newVhostArray['this_user']}'s IDs are: UID='{$this_users_IDs['UID']}' and GID='{$this_users_IDs['GID']}'\n";

if ( ! ( $this_groups_ID = getGroupInfo( $newVhostArray['this_group'] ) ) ) {
	post_errors( "Could not get data on Group '{$newVhostArray['this_group']}'!" );
	}
echo "In '/etc/group',  found Group '{$newVhostArray['this_group']}'s ID is: GID='$this_groups_ID'\n\n";

if ( $this_users_IDs['UID'] < MIN_USER_ID ) {
	post_errors( "UID for user '{$newVhostArray['this_user']}' is lower than the system allowable of: '" . MIN_USER_ID . "'!" );
	}

if ( $this_users_IDs['GID'] < MIN_GROUP_ID || $this_groups_ID < MIN_GROUP_ID ) {
	post_errors( "GID for user '{$newVhostArray['this_user']}' is lower than the system allowable of: '" . MIN_GROUP_ID . "'!" );
	}


$restart_server = 1;

//  Handle "secured"-option argument.
if ( $newVhostArray['is_secured'] ) {
    $newVhostArray['server_port'] = ( $newVhostArray['server_port'] ? $newVhostArray['server_port'] : 443 );

    $newVhostArray['cert_path'] = ( $newVhostArray['cert_path'] ? $newVhostArray['cert_path'] : '/etc/ssl' );
    $newVhostArray['cert_name'] = ( $newVhostArray['cert_name'] ? $newVhostArray['cert_name'] : $newVhostArray['server_name'] );

    if ( ! is_private_key( $newVhostArray ) || ! is_certificate( $newVhostArray ) ) {
        $restart_server = 0;
        warn_no_cert( $newVhostArray );
        }
    }

else {
    $newVhostArray['server_port'] = ( $newVhostArray['server_port'] ? $newVhostArray['server_port'] : 80 );
    }

//  Check is this user's "suexec" config file exists.
$this_SUEXEC = "/etc/apache2/suexec/{$newVhostArray['this_user']}";

if ( $newVhostArray['add_suexec'] && ! file_exists( $this_SUEXEC ) &&
    ! file_put_contents( $this_SUEXEC, gen_suexec_conf( $newVhostArray ) ) ) {

    post_errors( "The 'suexec' config file for user: '{$newVhostArray['this_user']}' does not exist to use suexec privileges!\n\tCould not create file '$this_SUEXEC'.\n\n( Is 'mod_suexec_custom' also installed and active, BTW? )" );
    }

//  Listen Port is useless without also specifying extra-option: 'php-fpm'. Disregard in such a case.
if ( $newVhostArray['listen_port'] && ! $newVhostArray['add_php_fpm'] ) {
    $newVhostArray['listen_port'] = false;
    echo "\nBecause the extra option 'php-fpm' was not selected, 'listen port' argument was ignored.\n";
    }

//  Begin the process of creating the new virtual host.
$thisVHost = "/etc/apache2/sites-available/{$newVhostArray['server_name']}.conf";

if ( file_exists( $thisVHost ) ) {
    post_errors( "The vHost for '{$newVhostArray['server_name']}' already exists! Terminating operation." );
    }

else {
//  Set the other default options' values (for those not already set).
//    $newVhostArray['this_user'] = ( $newVhostArray['this_user'] ? $newVhostArray['this_user'] : 'www-data' );
//    $newVhostArray['this_group'] = ( $newVhostArray['this_group'] ? $newVhostArray['this_group'] : $newVhostArray['this_user'] );
    $newVhostArray['server_ip'] = ( $newVhostArray['server_ip'] ? $newVhostArray['server_ip'] : '127.0.0.1' );

//  Make sure we check to see if a common "public folder" has been declared.
    $webRoot = get_webroot_folder( $newVhostArray['document_root'] );
    $newVhostArray['document_root'] = $webRoot . '/' . get_public_folder( $newVhostArray['document_root'] );
    $appHeader = "{$newVhostArray['document_root']} on: {$newVhostArray['server_name']}.";

    if ( $newVhostArray['add_php_fpm'] && ( $php_fpm_version = find_php_version() ) ) {
        $php_fpm_filename = str_replace( '.', '_', $newVhostArray['server_name'] );

        if ( ! file_put_contents( "/etc/php/$php_fpm_version/fpm/pool.d/$php_fpm_filename.conf",
            create_ht_hdr( 'php-fpm.conf', $newVhostArray['server_name'], ';', $THIS_APP['header'] ) . gen_phpfpm_pool( $newVhostArray ) ) ) {

            echo "WARNING:  Could not write the PHP-FPM configuration file for: '$serverName'! Not restarting the server.\n";
            $restart_server = 0;
            }

        else {
            send_to_shell( "systemctl restart php$php_fpm_version-fpm" );
            }
        }

        if ( ! file_put_contents( $thisVHost, create_ht_hdr( 'vhost.conf', $newVhostArray['server_name'], '#', $THIS_APP['header'] ) . create_vhost( $newVhostArray ) ) ) {

            echo "WARNING:  Could not write the vhost configuration file for: '{$newVhostArray['server_name']}'! Not restarting the server.\n";
            $restart_server = 0;
            }

    if ( check_hosts_file( $newVhostArray ) ) {
        echo "\nNOTE:   The 'hosts' file entry for: '{$newVhostArray['server_name']}' already exists.\n";
        }

    else {
        echo "\nAdding The 'hosts' file entry for: '{$newVhostArray['server_name']}'.\n";
        update_hosts_file( $newVhostArray, $THIS_APP['header'] );
        }

//  Create the user's webroot folder and populate it with intial starter files.
    if ( ! file_exists( $newVhostArray['document_root'] ) ) {
        populate_public_folder( $newVhostArray, $THIS_APP['header'] );
        }

    else {
        echo "\nNOTE:   The Document Root '{$newVhostArray['document_root']}', for: '{$newVhostArray['server_name']}' already exists!\n";
        }

//  If there were no serious errors, enable the new vhost and restart the Apache2 server.
    if ( $restart_server == 1 ) {
        echo "\nRestarting the server. '{$newVhostArray['server_name']}' should now be visible at IP '{$newVhostArray['server_ip']}'.\n";

//      send_to_shell( "a2ensite {$newVhostArray['server_name']} > /dev/null" );
        send_to_shell( "a2ensite {$newVhostArray['server_name']}" );
        send_to_shell( 'systemctl reload apache2' );
        }

    echo "\nAll Done!\n";
    }

