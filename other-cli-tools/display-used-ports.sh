#!/bin/bash


# sudo ss -tulpn
# sudo ss -tuln | grep LISTEN
# sudo lsof -i -P -n | grep LISTEN

echo
echo -e "Ports which already have\nactive listeners:"
echo '____________________________'

for P in $(sudo ss -tuln | grep LISTEN | grep -P -o ':([0-9]+)\ ' | grep -P -o '\d+') ; do
	echo -e "| Port: $P\tis in use. |"

#	if [ $1 -eq $P ] ; then
#		echo $P
#		exit 1
#	fi
done

echo '----------------------------'
echo

exit
