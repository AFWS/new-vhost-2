#!/bin/bash

function regex_check {
    echo $(echo $2 | grep -P -i "[^$1]+")
}

function isPort_used {
	if [ "x$(regex_check '0-9' $1)" != 'x' ] ; then
		echo 0
		exit
	fi

    for P in $(ss -tuln | grep LISTEN | grep -P -o ':([0-9]+)\ ' | grep -P -o '\d+') ; do
        if [ $1 -eq $P ] ; then
            echo $P
            exit 0
	    fi
    done

    echo 0
    exit
}


if [ $# -eq 0 ] ; then
	echo
	echo "Need to enter a port number."
	echo
	exit 2
fi

echo
echo "Argument Given: $1"
echo

test=$(isPort_used $1)

if [ $test -ne 0 ] ; then
	echo "Port: $test already has a listener."
else
	echo "This port is not used by a listerner yet."
fi

echo
