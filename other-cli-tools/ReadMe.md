These utility scripts can also come in very handy!

They may need to be run as "sudo" in order for them to be very useful (or even work at all).

They are just ordinary BASH scripts which do a few calls - and output the results to the terminal window.

